The Rules

* You have 10 minutes to discuss a plan with the team members
* You have 45 minutes to work on the solution
* You have 5 minutes to put all the pieces together

One Restriction!
During the 45 minutes, you're not allowed to communicate with your other team members in anyway! No cheaters!

What You Will Be Building

* A Connect 4 game in the browser
* Red and black peices will be used on a browser game board you will create
* Clicking a column will put a game piece on the board
* Every time a game piece is successfully placed, the next click will place the alternate color

What You Will Be Building (continued)

* Doesn't matter which color goes first
* When someone wins, you should show "Winner: Red!" or "Winner: Black!" on the screen
* Refreshing the page will refresh the game board, it does not need any persisted data
* If any doubt, follow the rules for Connect 4
