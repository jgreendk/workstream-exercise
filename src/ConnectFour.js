import React from 'react';
import {connect} from 'react-redux';

const ConnectFour = ({text}) => {
    return <div>{text}</div>;
}

const mapStateToProps = state => ({
    text: state.sampleText,
});

export default connect(mapStateToProps)(ConnectFour);