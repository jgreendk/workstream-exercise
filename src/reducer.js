const INITIAL_STATE = {
    sampleText: 'Connect Four!',
};

const reducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
    default:
        return state;
    }
};

export default reducer;